﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AltaUsuarios.Entidad
{
    public class Usuario
    {

        public string Nombre { get; set; }
        public int Edad { get; set; }
        public string Email { get; set; }
        public string Contraseña { get; set; }
    }
   
}
