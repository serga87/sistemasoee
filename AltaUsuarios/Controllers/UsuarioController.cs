﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AltaUsuarios.Models;
using AltaUsuarios.Entidad;

namespace AltaUsuarios.Controllers
{
    public class UsuarioController : Controller
    {
        public ActionResult Index(UsuarioVM usuarioVM)
        {

            return View("Alta", new UsuarioVM());
        }

        [HttpPost]
        public ActionResult Alta(UsuarioVM usuarioVM)
        {
            if (ModelState.IsValid)
            {
                bool modoBBDD;
                bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["GuardadoBBDD"], out modoBBDD);
                if (modoBBDD)
                {
                    //uso de bbdd
                }
                else
                {
                    var listadoUsuarios = Session["Usuarios"] != null ? (List<Usuario>)Session["Usuarios"] : new List<Usuario>();
                    if (listadoUsuarios.Any(x => x.Email.Equals(usuarioVM.Email)))
                    {
                        return View("Resultado", new ResultadoVM() { Mensaje = "Ya existe email." });
                    }
                    else
                    {
                        listadoUsuarios.Add(new Usuario() { Nombre = usuarioVM.Nombre, Contraseña = usuarioVM.Contraseña, Edad = usuarioVM.Edad, Email = usuarioVM.Email });
                        Session["Usuarios"] = listadoUsuarios;
                        return View("Resultado", new ResultadoVM() { Mensaje = "Inserción correcta." });
                    }
                }
                //guardar
            }

            return View();
        }

        public ActionResult Listado()
        {
            var listadoUsuarios = Session["Usuarios"] != null ? (List<Usuario>)Session["Usuarios"] : new List<Usuario>();
            var listadoUsuariosVm = new List<UsuarioVM>();
            foreach (var usuario in listadoUsuarios)
            {
                listadoUsuariosVm.Add(new UsuarioVM() { Nombre = usuario.Nombre, Edad = usuario.Edad, Email = usuario.Email });
            }
            return View(listadoUsuariosVm);
        }

        public ActionResult Login()
        {
            return View(new LoginVM());
        }

        [HttpPost]
        public ActionResult Login(LoginVM loginVM)
        {
            bool modoBBDD;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["GuardadoBBDD"], out modoBBDD);
            if (modoBBDD)
            {
                //leer de bbdd
            }
            else
            {
                var listadoUsuarios = Session["Usuarios"] != null ? (List<Usuario>)Session["Usuarios"] : new List<Usuario>();
                if (listadoUsuarios.Any(x => x.Email.Equals(loginVM.Email) && x.Contraseña.Equals(loginVM.Contraseña)))
                {
                    var listadoUsuariosVm = new List<UsuarioVM>();
                    foreach (var usuario in listadoUsuarios)
                    {
                        listadoUsuariosVm.Add(new UsuarioVM() { Nombre = usuario.Nombre, Edad = usuario.Edad, Email = usuario.Email });
                    }
                    return View("Listado", listadoUsuariosVm);
                }
                else
                {
                    loginVM.MensajeError = "Usuario o contraseña invalido.";
                    return View(loginVM);
                }
            }

            return View();
        }
    }
}