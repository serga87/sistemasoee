﻿using AltaUsuarios.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AltaUsuarios.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sesion()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult BBDD()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}