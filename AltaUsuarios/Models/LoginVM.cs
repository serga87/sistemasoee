﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AltaUsuarios.Models
{
    public class LoginVM
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Contraseña { get; set; }

        public string MensajeError { get; set; }
    }
}
