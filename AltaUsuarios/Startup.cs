﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AltaUsuarios.Startup))]
namespace AltaUsuarios
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
